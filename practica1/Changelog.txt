1.Modificarse la cabacera Mundo.h incorporando "haowei lu"
  codigo:
	luk@ubuntu:~/sii-51565/practica1$ cd include 
	luk@ubuntu:~/sii-51565/practica1/include$ ls
	Esfera.h  glut.h  Mundo.h  Plano.h  Raqueta.h  Vector2D.h
	luk@ubuntu:~/sii-51565/practica1/include$ gedit Mundo.h

2.Renombrarse el directorio practica4 a practica5
  codigo:
	luk@ubuntu:~/sii-51565$ ls
	practica1  practica4
	luk@ubuntu:~/sii-51565$ mv practica4 practica5
	luk@ubuntu:~/sii-51565$ ls
	practica1  practica5

3.Revisar el estado de gestion de los ficheros de git
  codigo:
	luk@ubuntu:~/sii-51565$ git status
	On branch master
	Your branch is up-to-date with 'origin/master'.
	Changes not staged for commit:
	  (use "git add/rm <file>..." to update what will be committed)
	  (use "git checkout -- <file>..." to discard changes in working directory)

		modified:   practica1/build/CMakeCache.txt
		modified:   practica1/build/CMakeFiles/Makefile.cmake
		modified:   practica1/include/Mundo.h
		deleted:    practica4/Socket.cpp
		deleted:    practica4/Socket.h

	Untracked files:
	  (use "git add <file>..." to include in what will be committed)

		practica1/Changelog.txt
		practica5/

	no changes added to commit (use "git add" and/or "git commit -a")

4.marcarse localmente los ficheros cambiados
  codigo:
	


